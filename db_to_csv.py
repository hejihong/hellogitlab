import csv
import os
import traceback
import uuid

import psycopg2


def get_connection(host='192.168.2.52', port='5432', user='writer', password='atman', database='writer', timeout=10):
    """Base the given params, return a connection object if no exception raised."""
    try:
        return psycopg2.connect(host=host, port=port, user=user, password=password, database=database,
                         connect_timeout=timeout)
    except psycopg2.Error as e:
        traceback.print_exc(e)
        return None


def get_data(query_sql):
    """Return the query result list."""
    if not query_sql or not isinstance(query_sql, str):
        query_sql = 'select "PMID", "TITLE", "KEYWORDS" from data_pubmed where "KEYWORDS" is not null' \
                    ' order by "ID" asc limit 5000 offset 0'

    # Get db connection
    connect = get_connection()
    if not connect:
        return []

    # Execute sql
    cursor = connect.cursor()
    try:
        cursor.execute(query_sql)
        return cursor.fetchall()
    except psycopg2.Error as e:
        traceback.print_exc(e)
        return []


def write_to_csv(data_row, filepath):
    with open(filepath, 'a+') as csv_file:
        pass


def batch_write_to_csv(data_rows, filepath):
    with open(filepath, 'a') as csv_file:
        csv_write = csv.writer(csv_file)
        csv_write.writerow(data_rows)


def keywords_parser(keywords_str):
    """Get the keyword list by parse the keywords str.

    Use function str.title() to unify the format of keyword,
    For example: 'Natural killer cells' -> 'Natural Killer Cells'.
    """
    return [str(keyword).strip().lower() for keyword in str(keywords_str).strip().split(';')]


def db_to_csv():
    # Create a result list generator for the query results.
    result_generator = (result for result in
                        get_data(
                            'select "PMID", "TITLE", "KEYWORDS" from data_pubmed where "KEYWORDS" is not null'
                            ' order by "ID" asc limit 5 offset 0'))
    if not result_generator:
        return

    documents, kw_doc_relationships, keywords = list(), list(), list()
    batch_len = 500
    for pmid, title, keywords_str in result_generator:

        if batch_len != 0:
            document_id = id_generator()
            documents.append((document_id, int(pmid), title, 'Doc'))
            for keyword in keywords_parser(keywords_str):
                keywords.append((id_generator(), keyword, 'Kw'))
                # Because of the same keyword appearing on different documents,
                # so we use the keyword`s name rather than the its generated id.
                # When to check if duplicated: when inserted into neo4j
                kw_doc_relationships.append((keyword, document_id))
            batch_len -= 1
        else:
            batch_write_to_csv(documents, 'csv_files/document-entities.csv')
            batch_write_to_csv(keywords, 'csv_files/keyword-entities.csv')
            batch_write_to_csv(kw_doc_relationships, 'csv_files/kw-doc-relationships.csv')
            documents.clear()
            keywords.clear()
            kw_doc_relationships.clear()
            batch_len = 500

        print(pmid, '\n', title, '\n', keywords_parser(keywords))


def id_generator():
    # Use uuid to generate a random code as an id.
    id_str = uuid.uuid4().hex
    id_lst = [id_str[:8], id_str[8:12], id_str[12:16], id_str[16:20], id_str[20:]]

    # Generate an id, example: 4c76ba47-272b-dd19-31b0-eb96efcb3958
    return '-'.join(id_lst)


db_to_csv()