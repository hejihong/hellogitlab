import csv
import traceback
import uuid

import psycopg2


def get_connection(host='192.168.2.52', port='5432', user='writer', password='atman',
                   database='writer', timeout=10):
    """Base on the given params, return a connection object if no exception raised."""
    try:
        return psycopg2.connect(host=host, port=port, user=user, password=password, database=database,
                         connect_timeout=timeout)
    except psycopg2.Error as e:
        traceback.print_exc(e)
        return None


def get_data(query_sql: str) -> list:
    """Base on the query sql, return the query result list."""
    if not query_sql or not isinstance(query_sql, str):
        query_sql = 'select "PMID", "TITLE", "KEYWORDS" from data_pubmed where "KEYWORDS" is not null' \
                    ' order by "ID" asc limit 5000 offset 0'

    # Get db connection.
    connect = get_connection()
    if not connect:
        return []

    # Execute sql and catch exception.
    cursor = connect.cursor()
    try:
        cursor.execute(query_sql)
        return cursor.fetchall()
    except psycopg2.Error as e:
        traceback.print_exc(e)
        return []


def write_lines_to_csv(data_rows: list, filepath: str):
    """Write data rows into csv file."""
    with open(filepath, 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerows(data_rows)


def keywords_parser(keywords_str: str) -> list:
    """Get the keyword list by parsing the keywords str.
    Use function str.capitalize() to unify the format of keyword,
    for example: 'Natural Killer CELL' -> 'Natural killer cell'.
    """
    return [str(keyword).strip().capitalize()
            for keyword in str(keywords_str).strip().split(';')]


def special_keywords_parser(keywords_str: str) -> list:
    """Use for parsing keywords str whose separator is abnormal or '\n' in keywords str.
    Abnormal separators: ['\n', '*', ...].
    """
    if ';' in keywords_str:
        if '\n' in keywords_str:
            keywords_str = keywords_str.replace('\n', '')
    else:
        if '\n' in keywords_str:
            pass

    return keywords_parser(keywords_str)


def id_generator() -> str:
    """Generate an id based on uuid.uuid4()."""
    # Use uuid to generate a random code as an id.
    id_str = uuid.uuid4().hex
    id_lst = [id_str[:8], id_str[8:12], id_str[12:16], id_str[16:20], id_str[20:]]

    # Generate an id, example: 4c76ba47-272b-dd19-31b0-eb96efcb3958.
    return '-'.join(id_lst)


def is_normal_keywords(keywords_str: str):
    # TODO(HeJihong): 暂定不含'\n'且以';'为分隔符的关键字字符串为正常示例
    return '\n' not in keywords_str and ';' in keywords_str


def data_parser(results: list) -> dict:
    """Base on the query result list, parse data from the result."""
    if not results:
        return dict()
    results = (result for result in results)

    # Parse data and add data list to dict.
    parsed_data = {
        'keywords': list(),
        'kw_doc_relationships': list(),
        'documents': list()
    }
    special_keywords = list()
    special_results = list()
    for pmid, title, keywords_str in results:
        # TODO(HeJihong): 此处仅处理keyword_str中不含'\n'的数据
        if is_normal_keywords(keywords_str):
            document_id = id_generator()
            parsed_data['documents'].append((document_id, pmid, str(title).strip(), 'Doc'))
            # Parse keywords_str, create keyword entity and kw-doc-relationship.
            for keyword in keywords_parser(keywords_str):
                # If keyword is already in the list, skip it; Otherwise, add it into list.
                if keyword not in (keyword_name for _, keyword_name, _ in parsed_data['keywords']):
                    parsed_data['keywords'].append((id_generator(), keyword, 'Kw'))
                # 考虑到keyword和document关系是基于neo4j中已有实体构建的，
                # 因此，使用从DB中的字段来作为keyword与document的关系构建的依据是最安全可靠的。
                # 即：根据keyword name查询keyword实体，根据pmid查询document实体。
                parsed_data['kw_doc_relationships'].append((keyword, pmid))
        else:
            special_results.append((pmid, title, keywords_str))
            special_keywords.append((keywords_str, 'separator'))
    write_lines_to_csv(special_keywords, '../csv_files/special-keywords-str.csv')

    return parsed_data


def db_to_csv():
    # Get query data and data length.
    results = get_data('select "PMID", "TITLE", "KEYWORDS" from data_pubmed where "KEYWORDS" is not null '
                       'order by "ID" asc limit 5000 offset 0')

    # Parse data.
    parsed_data = data_parser(results)

    # Write parsed data list to csv files.
    write_lines_to_csv(parsed_data['documents'], '../csv_files/document-entities.csv')
    write_lines_to_csv(parsed_data['keywords'], '../csv_files/keyword-entities.csv')
    write_lines_to_csv(parsed_data['kw_doc_relationships'], '../csv_files/kw-doc-relationships.csv')


if __name__ == '__main__':
    db_to_csv()
