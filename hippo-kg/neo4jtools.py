from neo4j.v1 import GraphDatabase

NEO4J_HOST = "bolt://localhost:7687"
NEO4J_USER = "neo4j"
NEO4J_PASSWORD = "neo4j"


class GraphConnector:
    """Defines connector for graph."""

    def __init__(self):
        self.driver = GraphDatabase.driver(NEO4J_HOST,
                                           auth=(NEO4J_USER, NEO4J_PASSWORD))

    def run(self, cypher):
        with self.driver.session() as session:
            return session.run(cypher)


def get_documents_cypher(documents_file: str):
    return f"USING PERIODIC COMMIT 500 " \
           f"LOAD CSV FROM '{documents_file}' AS row FIELDTERMINATOR ','" \
           f"WITH row[0] AS id, row[1] AS pmid, row[2] AS name, row[3] AS typeAlias " \
           f"MERGE (d:Document {{id: id, pmid: pmid, name: name, typeAlias: typeAlias}}) " \
           f"RETURN COUNT(*) AS count"


def get_keywords_cypher(keywords_file: str):
    return f"USING PERIODIC COMMIT 500 " \
           f"LOAD CSV FROM '{keywords_file}' AS row FIELDTERMINATOR ','" \
           f"WITH row[0] As id, row[1] AS name, row[2] AS typeAlias " \
           f"MERGE (k:Keyword {{id: id, name: name, typeAlias: typeAlias}}) " \
           f"RETURN COUNT(*) AS count"


def csv_to_neo4j(csv_file: str):
    graph = GraphConnector()
    cypher = get_documents_cypher(csv_file)
    result = graph.run(cypher)
    count = result.data()[0]['count']
    print(count)


if __name__ == '__main__':
    csv_to_neo4j('file:///keyword-entities.csv')
    csv_to_neo4j('file:///document-entities.csv')
